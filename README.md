# Context

This repository contains the BCARE Simulator. This project was created to validate the BCARE model for my final paper @ Unisinos. This simulator consists in a client for the BCARE model, it's capable to simulate the movement of users and generate and aleatory number of stress. This client generate the information that is sent to BCARE model and used to the Machine Learning process This project was built on Java SE 16. All the communication is realized by endpoints on HTTP protocol and JSON format. The project also use Maven for managing the the dependencies.

This project uses the BCARE Model as server that can be accessed by: https://bitbucket.org/brunoqmt/bcare-server

# Tooling

- **Java**: As the main language.
- **Eclipse IDE**: As the IDE to develop the model.
- **JSON**: As the format of communication.
- **UTF-8**: As the encoding.


# Variables that affect the results

The project uses a lot of variables and configurations that can be set to influence and improve the integrity of the results:

- **Interval of Execution**: The interval of execution could be selectec informing the months and the years. Then, the simulator with use the Calendar to retrieve days and week days.
- **Interval of Time**: The interval of time could be defined to inform when the routine will start and when it will finish.
- **Places**: For the simulation purposes, the Places must be created manually for the tests.
- **Coefficient of Stress**: Every place should receive it's coefficient of stress that allow to define places that are more stressfull than others.
- **Users and Users Routine**: For the simulation purposes, the Users must be created manually for the tests. It also allow to set the routine as where the user lives, works, study and the interval of time that it will be executed on the routine.
- **Category Time**: The category time allow to create rules for the places, like what time it will be open for interation and which days in the week.


# Architecture Detail Diagram

![01_Architecture_Detail](https://drive.google.com/uc?export=view&id=1qs9DRT79KccZQ_G-86HMibBD5M4lGCj-)


# Technologies

![02_Technologies_Detail](https://drive.google.com/uc?export=view&id=16IGsjhZLOyO6xvLqUv81Liu6dgIRempC)


# ER Diagram

![03_ER_Diagram](https://drive.google.com/uc?export=view&id=1Ik8t-bCJWH3FSm4o1rzHdLrvp9Wp8J4W)


# Precision Results

![04_Results](https://drive.google.com/uc?export=view&id=1uKAJo5DYZzg25s6O1qx9ZR7FifozxUEw)