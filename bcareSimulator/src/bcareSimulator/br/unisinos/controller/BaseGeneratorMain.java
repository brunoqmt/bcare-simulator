package bcareSimulator.br.unisinos.controller;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bcareSimulator.br.unisinos.model.bean.Days;
import bcareSimulator.br.unisinos.model.bean.Place;
import bcareSimulator.br.unisinos.model.bean.StressCondition;
import bcareSimulator.br.unisinos.model.bean.Trail;
import bcareSimulator.br.unisinos.model.bean.User;
import bcareSimulator.br.unisinos.model.dao.ConnectionDAO;
import bcareSimulator.br.unisinos.model.dao.PlaceDAO;
import bcareSimulator.br.unisinos.model.dao.StressConditionDAO;
import bcareSimulator.br.unisinos.model.dao.TrailDAO;
import bcareSimulator.br.unisinos.model.dao.UserDAO;
import bcareSimulator.br.unisinos.model.util.DateUtil;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class BaseGeneratorMain {

	private Connection connection = null;
	private List<StressCondition> stressConditions = null;
	private List<User> users;
	private List<Place> places;

//	private int[] years = { 2016, 2017, 2018, 2019, 2020};
	private int[] months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private int[] years = { 2015 };
//	private int[] months = { 1, 2, 3, 4, 5, 6 };

	private int firstHour = 6;
	private int lastHour = 24;

	private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private PlaceController placeController;
	private CoefficientController coefficientController;

	private TrailDAO trailDAO;

	/**
	 * Filling the main lists used to simulate
	 */
	public void preProcessment() {
		connection = new ConnectionDAO().connect();

		trailDAO = new TrailDAO(connection);

		stressConditions = new StressConditionDAO(connection).retrieveAll();
		users = new UserDAO(connection).retrieveAll();
		places = new PlaceDAO(connection).retrieveAll();

		placeController = new PlaceController(places);
		coefficientController = new CoefficientController(stressConditions);
	}

	/**
	 * The core of process
	 * 
	 * @throws ParseException
	 * @throws InterruptedException 
	 */
	public void processment() throws ParseException, InterruptedException {
		// cronological execution
		for (int y : years) {
			for (int m : months) {
				int days = new Days().retrieveDaysOnMonth(y, m);
				for (int i = 1; i <= days; i++) {
					System.out.println(i + "/" + m + "/" + y);
					for (int h = firstHour; h < lastHour; h++) {
						Date date = sdf.parse(y + "-" + m + "-" + i + " " + h + ":00:00");
						// users iteration
						for (User user : users) {
							// 1- find new place
							Place place = placeController.findPlace(date, user, firstHour, lastHour, h, true);
							// 2- calculate coef
							Trail trail = coefficientController.calculateCoef(place);
							// 3- set another attr
							trail.setUser(user);
							trail.setPlace(place);
							trail.setDatetimeMoment(date);
							trail.setDayOfWeek(new DateUtil().getDayWeek(date) + "");
							trail.setHour(h + "");
							// 4- persist trail
							trailDAO.insert(trail);
							
							Thread.sleep(5);
						}
//						System.out.println(h + ":00:00");
					}
//					System.out.println("");
				}
			}
		}
	}

	public static void main(String[] args) {
		BaseGeneratorMain main = new BaseGeneratorMain();

		long start = System.currentTimeMillis();
		try {
			main.preProcessment();
			main.processment();
		} catch (Exception e) {
			System.out.println("Fail on the main method. Message: " + e.getMessage());
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();

		System.out.println("");
		System.out.println("Tempo execução: " + (end - start) + " ms");

	}

}
