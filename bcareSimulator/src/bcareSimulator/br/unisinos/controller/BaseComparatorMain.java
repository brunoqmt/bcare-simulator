package bcareSimulator.br.unisinos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bcareSimulator.br.unisinos.model.bean.Days;
import bcareSimulator.br.unisinos.model.bean.Place;
import bcareSimulator.br.unisinos.model.bean.StressCondition;
import bcareSimulator.br.unisinos.model.bean.Trail;
import bcareSimulator.br.unisinos.model.bean.User;
import bcareSimulator.br.unisinos.model.client.BCareClient;
import bcareSimulator.br.unisinos.model.dao.ConnectionDAO;
import bcareSimulator.br.unisinos.model.dao.PlaceDAO;
import bcareSimulator.br.unisinos.model.dao.StressConditionDAO;
import bcareSimulator.br.unisinos.model.dao.TrailDAO;
import bcareSimulator.br.unisinos.model.dao.UserDAO;
import bcareSimulator.br.unisinos.model.util.DateUtil;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class BaseComparatorMain {

	private Connection connection = null;
	private List<StressCondition> stressConditions = null;
	private List<User> users;
	private List<Place> places;

//	private int[] years = { 2020, 2021};
//	private int[] months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private int[] years = { 2021 };
	private int[] months = { 2, 3, 4, 5, 6};

	private int firstHour = 6;
	private int lastHour = 24;

	private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private PlaceController placeController;
	private CoefficientController coefficientController;

	private BCareClient client;

	/**
	 * Filling the main lists used to simulate
	 */
	public void preProcessment() {
		connection = new ConnectionDAO().connect();

		stressConditions = new StressConditionDAO(connection).retrieveAll();
		users = new UserDAO(connection).retrieveAll();
		places = new PlaceDAO(connection).retrieveAll();

		placeController = new PlaceController(places);
		coefficientController = new CoefficientController(stressConditions);

		client = new BCareClient();
	}

	/**
	 * The core of process
	 * 
	 * @throws ParseException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void processment() throws ParseException, IOException, InterruptedException {
		// cronological execution
		for (int y : years) {
			for (int m : months) {
				int days = new Days().retrieveDaysOnMonth(y, m);
				for (int i = 1; i <= days; i++) {
					System.out.println(i + "/" + m + "/" + y);
					for (int h = firstHour; h < lastHour; h++) {
						System.out.println(h);
						Date date = sdf.parse(y + "-" + m + "-" + i + " " + h + ":00:00");
						// users iteration
						for (User user : users) {
							if (user.getId() == 37 || user.getId() == 47 || user.getId() == 72 || user.getId() == 77) {
//							if (user.getId() == 77) {
								// 1- find new place
								Place place = placeController.findPlace(date, user, firstHour, lastHour, h, false);
								// 2- calculate coef
								Trail trail = coefficientController.calculateCoef(place);
								// 3- set another attr
								trail.setUser(user);
								trail.setPlace(place);
								trail.setDatetimeMoment(date);
								trail.setDayOfWeek(new DateUtil().getDayWeek(date) + "");
								trail.setHour(h + "");
								// 4- persist trail
								Trail cloneTrail = new Trail();
								cloneTrail.setHour(trail.getHour());
								cloneTrail.setDatetimeMoment(trail.getDatetimeMoment());
								cloneTrail.setDayOfWeek(trail.getDayOfWeek());
								cloneTrail.getUser().setId(trail.getUser().getId());
								cloneTrail.getPlace().setId(trail.getPlace().getId());
								cloneTrail.getPlace().setDescription(trail.getPlace().getDescription());
								cloneTrail.setStressCondition(trail.getStressCondition());
								cloneTrail.setStressCoef(trail.getStressCoef());

								// 4 - Call WS (send the Trail, persist, retrieve)
								String response = client.callPersistTrailAndGetPrediction(cloneTrail);
								// 5 - Say something or generate alert...
								Thread.sleep(3);
							}
						}
					}
					System.out.println("");
				}
			}
		}
	}

	public static void main(String[] args) {
		BaseComparatorMain main = new BaseComparatorMain();

		long start = System.currentTimeMillis();
		try {
			main.preProcessment();
			main.processment();
		} catch (Exception e) {
			System.out.println("Fail on the main method. Message: " + e.getMessage());
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();

		System.out.println("");
		System.out.println("Tempo execução: " + (end - start) + " ms");

	}

}
