package bcareSimulator.br.unisinos.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bcareSimulator.br.unisinos.model.bean.Place;
import bcareSimulator.br.unisinos.model.bean.User;
import bcareSimulator.br.unisinos.model.util.DateUtil;
import bcareSimulator.br.unisinos.model.util.Util;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 6 de out. de 2021
 */
public class PlaceController {

	private List<Place> places = null;

	public PlaceController(List<Place> places) {
		this.places = places;
	}

	public Place findPlace(Date actualTime, User user, int firstHour, int lastHour, int nowHour, boolean isSimulation) throws ParseException {
		Place place;
		boolean isValid = false;

		/* Verify the Home Point to start and finalize the day */
		place = verifyFirstOrLastHour(firstHour, lastHour, nowHour, user);
		if (place != null) {
			return place;
		}

		/* Verify Routine */
		place = verifyTheRoutine(actualTime, user);

		/* Verify the Day of Week */
		if (place != null) {
			isValid = this.verifyTheDayWeek(place, actualTime);
		}

		/* If place found and routine matched, than exit, otherwise continues */
		if (place != null && isValid) {
			return place;
		}

		do {
			int randomIndex = new Util().newRandom(0, places.size());
			place = places.get(randomIndex);

			if (isSimulation) {
				if (place.getId() != user.getExclusionPlace().getId()) {
					/* Verify the Days of Week */
					isValid = this.verifyTheDayWeek(place, actualTime);
					/* Verify the time range inside the correct day */
					if (isValid) {
						isValid = this.verifyTheTimeRange(place.getCategoryTime().getOpeningTime(), place.getCategoryTime().getClosingTime(), actualTime);
					}
				} else {
					isValid = false;
				}
			} else {
				/* Verify the Days of Week */
				isValid = this.verifyTheDayWeek(place, actualTime);
				/* Verify the time range inside the correct day */
				if (isValid) {
					isValid = this.verifyTheTimeRange(place.getCategoryTime().getOpeningTime(), place.getCategoryTime().getClosingTime(), actualTime);
				}
			}
		} while (!isValid);

		return place;
	}

	private Place verifyFirstOrLastHour(int firstHour, int lastHour, int nowHour, User user) {
		if (firstHour == nowHour) {
			return this.findByID(user.getHomePoint().getId());
		} else if ((lastHour - 1) == nowHour) {
			return this.findByID(user.getHomePoint().getId());
		}
		return null;
	}

	private Place verifyTheRoutine(Date actualTime, User user) throws ParseException {
		if (user.getWorkPoint().getId() != null && user.getWorkPoint().getId() > 0) {
			Boolean itsTimeToWork = verifyTheTimeRange(user.getWorkStartTime(), user.getWorkEndTime(), actualTime);
			if (itsTimeToWork) {
				return this.findByID(user.getWorkPoint().getId());
			}
		}
		if (user.getStudyPoint().getId() != null && user.getStudyPoint().getId() > 0) {
			Boolean itsTimeToStudy = verifyTheTimeRange(user.getStudyStartTime(), user.getStudyEndTime(), actualTime);
			if (itsTimeToStudy) {
				return this.findByID(user.getStudyPoint().getId());
			}
		}
		return null;
	}

	public Place findByID(Integer id) {
		for (Place place : places) {
			if (place.getId() == id) {
				return place;
			}
		}
		return null;
	}

	private Boolean verifyTheTimeRange(Date startTime, Date endTime, Date actualTime) throws ParseException {
		boolean isValid = false;
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

		actualTime = timeFormat.parse(timeFormat.format(actualTime));
		startTime = timeFormat.parse(timeFormat.format(startTime));
		endTime = timeFormat.parse(timeFormat.format(endTime));

		/* Verify the time range */
		if (actualTime.compareTo(startTime) > 0 && actualTime.compareTo(endTime) < 0) {
			/* The actual time is after start time AND the actual time is before endTime */
			isValid = true;
		} else if (actualTime.compareTo(startTime) == 0 || actualTime.compareTo(endTime) == 0) {
			/*
			 * The actual time is equals to the start time OR the actual time is equals to
			 * endTime
			 */
			isValid = true;
		}
		return isValid;
	}

	private Boolean verifyTheDayWeek(Place place, Date actualTime) {
		Boolean isValid = false;
		int dayOfWeek = new DateUtil().getDayWeek(actualTime);

		switch (dayOfWeek) {
		case 1:
			if (place.getCategoryTime().getSunday()) {
				isValid = true;
			}
			break;
		case 2:
			if (place.getCategoryTime().getMonday()) {
				isValid = true;
			}
			break;
		case 3:
			if (place.getCategoryTime().getTuesday()) {
				isValid = true;
			}
			break;
		case 4:
			if (place.getCategoryTime().getWednesday()) {
				isValid = true;
			}
			break;
		case 5:
			if (place.getCategoryTime().getThursday()) {
				isValid = true;
			}
			break;
		case 6:
			if (place.getCategoryTime().getFriday()) {
				isValid = true;
			}
			break;
		case 7:
			if (place.getCategoryTime().getSaturday()) {
				isValid = true;
			}
			break;
		default:
			break;
		}
		return isValid;
	}

}
