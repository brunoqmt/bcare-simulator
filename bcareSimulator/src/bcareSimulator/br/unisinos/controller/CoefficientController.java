package bcareSimulator.br.unisinos.controller;

import java.util.List;

import bcareSimulator.br.unisinos.model.bean.Place;
import bcareSimulator.br.unisinos.model.bean.StressCondition;
import bcareSimulator.br.unisinos.model.bean.Trail;
import bcareSimulator.br.unisinos.model.util.Util;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 6 de out. de 2021
 */
public class CoefficientController {

	private List<StressCondition> stressConditions = null;

	public CoefficientController(List<StressCondition> stressConditions) {
		this.stressConditions = stressConditions;
	}

	public Trail calculateCoef(Place place) {
		Trail trail = new Trail();

		int randCoeff = new Util().newRandom(1, 10);
		Double coefficient = randCoeff * place.getCoefficient();
		coefficient = new Util().truncateDecimal(coefficient, 1);
		
		StressCondition sc = this.findRespectiveCondition(coefficient);

		trail.setStressCoef(coefficient);
		trail.setStressCondition(sc);

		return trail;
	}



	public StressCondition findRespectiveCondition(Double coefficient) {
		StressCondition sc = null;
		for (StressCondition stressCondition : stressConditions) {
			if (stressCondition.getCoefficientStart() <= coefficient && stressCondition.getCoefficientEnd() >= coefficient) {
				sc = stressCondition;
			}
		}
		return sc;
	}

}
