package bcareSimulator.br.unisinos.model.bean;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class StressCondition {

	private Integer id;
	private double coefficientStart;
	private double coefficientEnd;
	private Integer statusCondition;
	private String statusConditionText;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getCoefficientStart() {
		return coefficientStart;
	}

	public void setCoefficientStart(double coefficientStart) {
		this.coefficientStart = coefficientStart;
	}

	public double getCoefficientEnd() {
		return coefficientEnd;
	}

	public void setCoefficientEnd(double coefficientEnd) {
		this.coefficientEnd = coefficientEnd;
	}

	public Integer getStatusCondition() {
		return statusCondition;
	}

	public void setStatusCondition(Integer statusCondition) {
		this.statusCondition = statusCondition;
	}

	public String getStatusConditionText() {
		return statusConditionText;
	}

	public void setStatusConditionText(String statusConditionText) {
		this.statusConditionText = statusConditionText;
	}

}
