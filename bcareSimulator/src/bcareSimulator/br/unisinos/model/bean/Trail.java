package bcareSimulator.br.unisinos.model.bean;

import java.util.Date;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 5 de out. de 2021
 */
public class Trail {

	private Integer id;
	private User user = new User();
	private Place place = new Place();
	private Date datetimeMoment;
	private String hour;
	private double stressCoef;
	private StressCondition stressCondition = new StressCondition();
	private String dayOfWeek;
	
	public Trail() {
	}
	
	public Trail(Trail t) {
		this.id = t.id;
		this.user = t.user;
		this.place = t.place;
		this.datetimeMoment = t.datetimeMoment;
		this.hour = t.hour;
		this.stressCoef = t.stressCoef;
		this.stressCondition = t.stressCondition;
		this.dayOfWeek = t.dayOfWeek;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Date getDatetimeMoment() {
		return datetimeMoment;
	}

	public void setDatetimeMoment(Date datetimeMoment) {
		this.datetimeMoment = datetimeMoment;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public double getStressCoef() {
		return stressCoef;
	}

	public void setStressCoef(double stressCoef) {
		this.stressCoef = stressCoef;
	}

	public StressCondition getStressCondition() {
		return stressCondition;
	}

	public void setStressCondition(StressCondition stressCondition) {
		this.stressCondition = stressCondition;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
}
