package bcareSimulator.br.unisinos.model.bean;

import java.time.YearMonth;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 7 de set. de 2021
 */
public class Days {

	public int retrieveDaysOnMonth(int year, int month) {
		YearMonth yearMonthObject = YearMonth.of(year, month);
		int days = yearMonthObject.lengthOfMonth();
		return days;
	}

}
