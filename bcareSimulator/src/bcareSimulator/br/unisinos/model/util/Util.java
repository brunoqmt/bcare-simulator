package bcareSimulator.br.unisinos.model.util;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 6 de out. de 2021
 */
public class Util {

	/**
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public int newRandom(int min, int max) {
		max = max - 1;
		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}

	public double truncateDecimal(Double value, int decimalPlaces) {
		value = value * Math.pow(10, decimalPlaces);
		value = Math.floor(value);
        value = value / Math.pow(10, decimalPlaces);
		return value;
	}
}
