package bcareSimulator.br.unisinos.model.util;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 11 de out. de 2021
 */
public class DateUtil {

	/**
	 * Return the integer of day on week, start sunday as 1
	 * 
	 * @param date
	 */
	public Integer getDayWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		return dayOfWeek;
	}

}
