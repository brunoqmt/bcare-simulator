package bcareSimulator.br.unisinos.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bcareSimulator.br.unisinos.model.bean.StressCondition;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  6 de out. de 2021
 */
public class StressConditionDAO {

	private Connection connection;

	public StressConditionDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Return a list of objects
	 * 
	 * @return List<StressCondition>
	 */
	public List<StressCondition> retrieveAll() {
		List<StressCondition> stressConditions = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM stress_condition";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				StressCondition sc = new StressCondition();
				sc.setId(rs.getInt("id"));
				sc.setCoefficientStart(rs.getDouble("coef_start"));
				sc.setCoefficientEnd(rs.getDouble("coef_end"));
				sc.setStatusCondition(rs.getInt("status_condition"));
				sc.setStatusConditionText(rs.getString("status_condition_text"));
				/* Add to list */
				stressConditions.add(sc);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Stress Conditions. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return stressConditions;
	}

	/**
	 * Return the match with ID
	 * 
	 * @param id
	 * @return StressCondition
	 */
	public StressCondition retrieveByID(Integer id) {
		StressCondition sc = new StressCondition();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM stress_condition WHERE id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				sc.setId(rs.getInt("id"));
				sc.setCoefficientStart(rs.getDouble("coef_start"));
				sc.setCoefficientEnd(rs.getDouble("coef_end"));
				sc.setStatusCondition(rs.getInt("status_condition"));
				sc.setStatusConditionText(rs.getString("status_condition_text"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Stress Condition by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return sc;
	}

	/**
	 * Insert the StressCondition on Database.
	 * 
	 * @param connection
	 * @param user (to be inserted)
	 * @return ID generated OR null for fail cases
	 */
	public Integer insert(Connection connection, StressCondition sc) {
		String sql = "INSERT INTO stress_condition(coef_start, coef_end, status_condition, status_condition_text) VALUES (?, ?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setDouble(1, sc.getCoefficientStart());
			pstmt.setDouble(2, sc.getCoefficientEnd());
			pstmt.setInt(3, sc.getStatusCondition());
			pstmt.setString(4, sc.getStatusConditionText());
			
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Stress Condition. Message: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Delete the StressCondition using an ID
	 * @param connection
	 * @param id
	 * @return true/false
	 */
	public Boolean deleteByID(Connection connection, Integer id) {
		String sql = "DELETE FROM stress_condition WHERE id = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Stress Condition by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
