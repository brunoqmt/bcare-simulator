package bcareSimulator.br.unisinos.model.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bcareSimulator.br.unisinos.model.bean.Trail;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  19 de out. de 2021
 */
public class BCareClient {
	
	private String url = "http://localhost:8080/bcareserver/nb/persistAndPrediction";
	private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	public String callPersistTrailAndGetPrediction(Trail trail) throws IOException {
		String response = this.get(trail);
		String data = buildResponse(response);
		return data;
	}
	

	private String buildResponse(String response) {
		// deal with the return
		return response;
	}

	private String get(Trail trail) throws IOException {
		URL urlConnection = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
		connection.setDoOutput(true);
	    connection.setRequestMethod("GET"); 
		connection.addRequestProperty("trail", gson.toJson(trail, Trail.class));
		connection.addRequestProperty("Content-Type", "application/json");
		connection.setConnectTimeout(30000);
		connection.setReadTimeout(30000);
		String response = "";
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"))) {
			String line;
			while ((line = reader.readLine()) != null) {
				response += line;
			}
		}
		return response;
	}

}
